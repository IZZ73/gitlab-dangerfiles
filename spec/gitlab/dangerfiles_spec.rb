# frozen_string_literal: true

RSpec.describe Gitlab::Dangerfiles do
  include_context "with dangerfile"

  let(:helper_plugin) { dangerfile.plugins[Danger::Helper] }
  let(:danger_plugin) { dangerfile.plugins[Danger::DangerfileDangerPlugin] }

  before do
    # Danger's import_plugin method does two things:
    #
    # 1. It requires the plugin path
    # 2. It performs a check to see whether the plugin count has changed
    #
    # This is all global state so doesn't play well with tests where this
    # code is called many times over.
    #
    # We therefore stub out the logic that performs the plugin load validation
    # and reduce it to a simple require.
    allow(danger_plugin).to receive(:import_plugin).and_wrap_original do |_, plugins_path|
      Dir.glob(plugins_path).sort.each { |path| require path }
    end
  end

  describe ".for_project" do
    it "yields an Engine instance" do
      expect { |b| described_class.for_project(dangerfile, &b) }.to(
        yield_with_args(be_instance_of(described_class::Engine))
      )
    end

    it "returns an Engine instance" do
      expect(described_class.for_project(dangerfile) { nil }).to be_instance_of(described_class::Engine)
    end

    context "when passing a project_name" do
      it "configures the helper with it" do
        engine = described_class.for_project(dangerfile, "my-project") { nil }

        expect(engine.config.project_name).to eq("my-project")
      end
    end

    context "when not passing a project name" do
      it "uses the helper config default" do
        engine = described_class.for_project(dangerfile) { nil }

        expect(engine.config.project_name).to eq(helper_plugin.config.project_name)
      end
    end
  end

  describe ".import_dangerfiles" do
    before do
      allow(dangerfile.helper).to receive(:release_automation?).and_return(false)
    end

    context "when helper.release_automation? is true" do
      before do
        allow(dangerfile.helper).to receive(:release_automation?).and_return(true)
      end

      it "does not import any rules" do
        expect(danger_plugin).not_to receive(:import_dangerfile)

        described_class.for_project(dangerfile) { |e| e.import_dangerfiles }
      end
    end

    it "imports all rules by default" do
      described_class.for_project(dangerfile) do |engine|
        engine.__send__(:all_gem_rules).each do |_rule, path|
          expect(danger_plugin).to receive(:import_dangerfile).with(path: path)
        end

        engine.import_dangerfiles
      end
    end

    context "with only: %w[changes_size]" do
      it "imports the :only rules" do
        expect(danger_plugin).to receive(:import_dangerfile).with(path: File.join(described_class::RULES_DIR, "changes_size"))

        described_class.for_project(dangerfile) { |e| e.import_dangerfiles(only: %w[changes_size]) }
      end
    end

    context "with except: %w[commit_messages]" do
      it "does not import the given rules" do
        expect(danger_plugin).not_to receive(:import_dangerfile).with(path: File.join(described_class::RULES_DIR, "commit_messages"))

        described_class.for_project(dangerfile) { |e| e.import_dangerfiles(except: %w[commit_messages]) }
      end
    end

    context "with only: %w[changes_size] and except: %w[commit_messages]" do
      it "imports the :only rules but not the :except rules" do
        expect(danger_plugin).to receive(:import_dangerfile).with(path: File.join(described_class::RULES_DIR, "changes_size"))

        described_class.for_project(dangerfile) { |e| e.import_dangerfiles(only: %w[changes_size], except: %w[commit_messages]) }
      end
    end

    context "with only: %w[changes_size] and except: %w[changes_size]" do
      it "imports no rules" do
        expect(danger_plugin).not_to receive(:import_dangerfile)

        described_class.for_project(dangerfile) { |e| e.import_dangerfiles(only: %w[changes_size], except: %w[changes_size]) }
      end
    end
  end

  describe described_class::Engine do
    let(:engine) { described_class.new(dangerfile) }

    describe "#config" do
      it "proxies to helper_plugin.config" do
        expect(engine.config).to eq(helper_plugin.config)
      end
    end

    describe "#filtered_rules" do
      let(:project_root) { "/foo" }
      let(:gem_root) { File.expand_path("../..", __dir__) }

      before do
        allow(engine).to receive(:config).and_return(double(project_root: project_root))

        # Mock gem's rules
        allow(Dir).to receive(:glob).with("#{Gitlab::Dangerfiles::RULES_DIR}/*").and_call_original
        Dir.glob(File.join(Gitlab::Dangerfiles::RULES_DIR, "*")).each do |path|
          allow(File).to receive(:directory?).with(File.expand_path(path)).and_call_original
          allow(File).to receive(:exist?).with(File.expand_path("#{path}/Dangerfile")).and_call_original
        end

        # Mock "project" custom rules
        allow(File).to receive(:directory?).with("#{project_root}/danger/ce_ee_vue_templates").and_return(true)
        allow(File).to receive(:exist?).with("#{project_root}/danger/ce_ee_vue_templates/Dangerfile").and_return(true)
        allow(Dir).to receive(:glob).with("#{project_root}/danger/*").and_return(%W[
                        #{project_root}/danger/ce_ee_vue_templates
                        #{project_root}/danger/not_a_dir
                        #{project_root}/danger/no_dangerfile
                      ])

        # Mock non existing dir
        allow(File).to receive(:directory?).with("#{project_root}/danger/not_a_dir").and_return(false)

        # Mock non existing Dangerfile
        allow(File).to receive(:directory?).with("#{project_root}/danger/no_dangerfile").and_return(true)
        allow(File).to receive(:exist?).with("#{project_root}/danger/no_dangerfile/Dangerfile").and_return(false)
      end

      it "proxies to helper_plugin.config" do
        expected_rules = {
          "ce_ee_vue_templates" => "#{project_root}/danger/ce_ee_vue_templates",
          "changelog" => "#{gem_root}/lib/danger/rules/changelog",
          "changes_size" => "#{gem_root}/lib/danger/rules/changes_size",
          "commit_messages" => "#{gem_root}/lib/danger/rules/commit_messages",
          "commits_counter" => "#{gem_root}/lib/danger/rules/commits_counter",
          "metadata" => "#{gem_root}/lib/danger/rules/metadata",
          "simple_roulette" => "#{gem_root}/lib/danger/rules/simple_roulette",
          "type_label" => "#{gem_root}/lib/danger/rules/type_label",
          "z_add_labels" => "#{gem_root}/lib/danger/rules/z_add_labels",
          "z_retry_link" => "#{gem_root}/lib/danger/rules/z_retry_link",
        }

        expect(engine.__send__(:filtered_rules, nil, [])).to eq(expected_rules)
        # Ensure order is correct
        expect(engine.__send__(:filtered_rules, nil, []).keys).to eq(expected_rules.keys)
      end
    end
  end
end

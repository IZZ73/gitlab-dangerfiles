# frozen_string_literal: true

require "spec_helper"

require "gitlab/dangerfiles/changes"

RSpec.describe Gitlab::Dangerfiles::Changes, :aggregate_failures do
  let(:added_file) { "doc/doc1" }

  subject do
    described_class.new([]).tap do |changes|
      changes << Gitlab::Dangerfiles::Change.new(added_file, :added, :docs)
      changes << Gitlab::Dangerfiles::Change.new("foo", :added, :none)
    end
  end

  describe "#has_category?" do
    it "returns true when changes include given category, false otherwise" do
      expect(subject.has_category?(:docs)).to eq(true)
      expect(subject.has_category?(:nils)).to eq(false)
    end
  end

  describe "#by_category" do
    it "returns an array of Change objects" do
      expect(subject.by_category(:docs)).to all(be_an(Gitlab::Dangerfiles::Change))
    end

    it "returns an array of Change objects with the given category" do
      expect(subject.by_category(:docs).files).to eq([added_file])
      expect(subject.by_category(:none).files).to eq(["foo"])
    end
  end

  describe "#categories" do
    it "returns an array of category symbols" do
      expect(subject.categories).to contain_exactly(:docs, :none)
    end
  end

  describe "#files" do
    it "returns an array of files" do
      expect(subject.files).to include(added_file)
    end

    context "with the same file assigned to multiple categories" do
      before do
        described_class.new([]).tap do |changes|
          changes << Gitlab::Dangerfiles::Change.new(added_file, :added, :none)
        end
      end

      it "lists the file only once" do
        expect(subject.files).to match_array([added_file, "foo"])
      end
    end
  end
end

# frozen_string_literal: true

require "spec_helper"

require "gitlab/dangerfiles/category"
require "gitlab/dangerfiles/teammate"

RSpec.describe Gitlab::Dangerfiles::Category do
  subject do
    described_class.for(name, project: project, kind: kind, labels: labels)
  end

  let(:project) { "gitlab" }
  let(:kind) { :reviewer }
  let(:labels) { [] }

  let(:teammate) do
    Gitlab::Dangerfiles::Teammate.new(
      "projects" => capabilities, "role" => role, "specialty" => specialty,
    )
  end

  let(:capabilities) { { "gitlab" => capability } }
  let(:capability) { "reviewer" }
  let(:role) { "Engineer" }
  let(:specialty) { nil }

  shared_examples "having capability" do
    it "has capability" do
      expect(subject.has_capability?(teammate)).to be(true)
    end
  end

  shared_examples "not having capability" do
    it "does not have capability" do
      expect(subject.has_capability?(teammate)).to be(false)
    end
  end

  context "when name is none" do
    let(:name) { :none }

    it_behaves_like "having capability"
  end

  context "when name is backend" do
    let(:name) { :backend }

    it_behaves_like "not having capability"

    context "when teammate has backend capability" do
      let(:capability) { "reviewer backend" }

      it_behaves_like "having capability"
    end
  end

  context "when name is test" do
    let(:name) { :test }

    it_behaves_like "not having capability"

    context "when role is Software Engineer in Test, Create:Source Code" do
      let(:role) { "Software Engineer in Test, Create:Source Code" }

      it_behaves_like "not having capability"

      context "when it has ~\"devops::create\" label" do
        let(:labels) { ["devops::create"] }

        it_behaves_like "having capability"
      end
    end
  end

  context "when name is tooling" do
    let(:name) { :tooling }

    it_behaves_like "not having capability"

    context "when teammate has tooling capability" do
      let(:capability) { "reviewer tooling" }

      it_behaves_like "having capability"
    end

    context "when teammate has backend capability" do
      let(:capability) { "reviewer backend" }

      it_behaves_like "having capability"

      context "when kind is maintainer" do
        let(:kind) { :maintainer }

        it_behaves_like "not having capability"
      end
    end
  end

  context "when name is import_integrate_be" do
    let(:name) { :import_integrate_be }

    it_behaves_like "not having capability"

    context "when role is Backend Engineer, Manage:Import and Integrate" do
      let(:role) { "Backend Engineer, Manage:Import and Integrate" }

      it_behaves_like "having capability"

      context "when role is Frontend Engineer, Manage:Import and Integrate" do
        let(:role) { "Frontend Engineer, Manage:Import and Integrate" }

        it_behaves_like "not having capability"
      end

      context "when kind is maintainer" do
        let(:kind) { :maintainer }

        it_behaves_like "not having capability"
      end
    end
  end

  context "when name is import_integrate_fe" do
    let(:name) { :import_integrate_fe }

    it_behaves_like "not having capability"

    context "when role is Frontend Engineer, Manage:Import and Integrate" do
      let(:role) { "Frontend Engineer, Manage:Import and Integrate" }

      it_behaves_like "having capability"

      context "when role is Backend Engineer, Manage:Import and Integrate" do
        let(:role) { "Backend Engineer, Manage:Import and Integrate" }

        it_behaves_like "not having capability"
      end

      context "when kind is maintainer" do
        let(:kind) { :maintainer }

        it_behaves_like "not having capability"
      end
    end
  end

  context "when name is ux" do
    let(:name) { :ux }

    it_behaves_like "not having capability"

    context "when teammate has ux capability" do
      let(:capability) { "reviewer ux" }

      it_behaves_like "having capability"

      context "when it also has ~\"group::source code\" label" do
        before do
          labels << "group::source code"
        end

        it_behaves_like "having capability"

        context "when specialty is Source Code" do
          let(:specialty) { "Source Code" }

          it_behaves_like "not having capability"
        end
      end

      context "when it has ~\"Community contribution\" label" do
        let(:labels) { ["Community contribution"] }

        it_behaves_like "not having capability"

        context "when it also has ~\"group::source code\" label" do
          before do
            labels << "group::source code"
          end

          it_behaves_like "not having capability"

          context "when role is Product Designer" do
            let(:role) { "Product Designer" }

            it_behaves_like "not having capability"

            context "when specialty is Source Code" do
              let(:specialty) { "Source Code" }

              it_behaves_like "having capability"
            end

            context "when specialty is Create: Source Code" do
              let(:specialty) { "Create: Source Code" }

              it_behaves_like "having capability"
            end

            context "when specialty is Create:Source Code" do
              let(:specialty) { "Create:Source Code" }

              it_behaves_like "having capability"
            end

            context "when specialty is Growth: Activation and Source Code" do
              let(:specialty) { ["Growth: Activation", "Source Code"] }

              it_behaves_like "having capability"
            end

            context "when specialty is Source Code, Growth:Activation" do
              let(:specialty) { [" Source Code", "Growth:Activation"] }

              it_behaves_like "having capability"
            end
          end
        end
      end
    end
  end
end

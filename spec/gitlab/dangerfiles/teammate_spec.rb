# frozen_string_literal: true

require "spec_helper"

require "gitlab/dangerfiles/teammate"

RSpec.describe Gitlab::Dangerfiles::Teammate do
  using RSpec::Parameterized::TableSyntax

  subject { described_class.new(options) }

  let(:tz_offset_hours) { 2.0 }
  let(:options) do
    {
      "username" => "luigi",
      "name" => "Luigi",
      "projects" => projects,
      "role" => role,
      "specialty" => specialty,
      "markdown_name" => "[Luigi](https://gitlab.com/luigi) (`@luigi`)",
      "tz_offset_hours" => tz_offset_hours,
    }
  end

  let(:capabilities) { ["reviewer backend"] }
  let(:projects) { { project => capabilities } }
  let(:role) { "Engineer, Manage" }
  let(:specialty) { nil }
  let(:labels) { [] }
  let(:project) { "gitlab" }

  describe "#==" do
    it "compares Teammate username" do
      joe1 = described_class.new("username" => "joe", "projects" => projects)
      joe2 = described_class.new("username" => "joe", "projects" => [])
      jane1 = described_class.new("username" => "jane", "projects" => projects)
      jane2 = described_class.new("username" => "jane", "projects" => [])

      expect(joe1).to eq(joe2)
      expect(jane1).to eq(jane2)
      expect(jane1).not_to eq(nil)
      expect(described_class.new("username" => nil)).not_to eq(nil)
    end
  end

  describe "#to_h" do
    it "returns the given options" do
      expect(subject.to_h).to eq(options)
    end
  end

  context "when having multiple capabilities" do
    let(:capabilities) { ["reviewer backend", "maintainer frontend", "trainee_maintainer qa", "reviewer ux"] }

    it "#reviewer? supports multiple roles per project" do
      expect(subject.reviewer?(project, :backend, labels)).to be_truthy
    end

    it "#reviewer? supports multiple roles per project" do
      expect(subject.reviewer?(project, :ux, labels)).to be_truthy
    end

    it "#traintainer? supports multiple roles per project" do
      expect(subject.traintainer?(project, :qa, labels)).to be_truthy
    end

    it "#maintainer? supports multiple roles per project" do
      expect(subject.maintainer?(project, :frontend, labels)).to be_truthy
    end

    context "when labels contain ~\"group::source code\"" do
      let(:labels) { ["group::source code"] }

      context "when the contribution is from wider community" do
        before do
          labels << "Community contribution"
        end

        context "when specialty contains Create:Source Code" do
          let(:specialty) { "Create:Source Code" }

          it "#reviewer? returns true" do
            expect(subject.reviewer?(project, :ux, labels)).to be_truthy
          end
        end

        context "when specialty contains, Configure" do
          let(:specialty) { "Configure" }

          it "#reviewer? returns false" do
            expect(subject.reviewer?(project, :ux, labels)).to be_falsey
          end
        end
      end
    end

    context "when labels contain devops::create" do
      let(:labels) { ["devops::create"] }

      context "when category is test and role is Software Engineer in Test, Create" do
        let(:role) { "Software Engineer in Test, Create" }

        it "#reviewer? returns true" do
          expect(subject.reviewer?(project, :test, labels)).to be_truthy
        end

        it "#maintainer? returns false" do
          expect(subject.maintainer?(project, :test, labels)).to be_falsey
        end

        context "when hyperlink is mangled in the role" do
          let(:role) { '<a href="#">Software Engineer in Test</a>, Create' }

          it "#reviewer? returns true" do
            expect(subject.reviewer?(project, :test, labels)).to be_truthy
          end
        end
      end
    end

    context "when role is Software Engineer in Test" do
      let(:role) { "Software Engineer in Test" }

      it "#reviewer? returns false" do
        expect(subject.reviewer?(project, :test, labels)).to be_falsey
      end
    end

    context "when role is Software Engineer in Test, Manage" do
      let(:role) { "Software Engineer in Test, Manage" }

      it "#reviewer? returns false" do
        expect(subject.reviewer?(project, :test, labels)).to be_falsey
      end
    end

    context "when capabilities include 'reviewer tooling'" do
      let(:capabilities) { ["reviewer tooling"] }

      it "#reviewer? returns true" do
        expect(subject.reviewer?(project, :tooling, labels)).to be_truthy
      end

      it "#maintainer? returns false" do
        expect(subject.maintainer?(project, :tooling, labels)).to be_falsey
      end
    end

    context "when capabilities include 'maintainer tooling'" do
      let(:capabilities) { ["maintainer tooling"] }

      it "#reviewer? returns false" do
        expect(subject.reviewer?(project, :tooling, labels)).to be_falsey
      end

      it "#maintainer? returns true" do
        expect(subject.maintainer?(project, :tooling, labels)).to be_truthy
      end
    end

    context "when capabilities include 'reviewer backend'" do
      let(:capabilities) { ["reviewer backend"] }

      it "#reviewer? returns true" do
        expect(subject.reviewer?(project, :tooling, labels)).to be_truthy
      end

      it "#maintainer? returns false" do
        expect(subject.maintainer?(project, :tooling, labels)).to be_falsey
      end
    end

    context "when capabilities include 'maintainer backend'" do
      let(:capabilities) { ["maintainer backend"] }

      it "#reviewer? returns false" do
        expect(subject.reviewer?(project, :tooling, labels)).to be_falsey
      end

      it "#maintainer? returns false" do
        expect(subject.maintainer?(project, :tooling, labels)).to be_falsey
      end
    end

    context "when capabilities include 'trainee_maintainer backend'" do
      let(:capabilities) { ["trainee_maintainer backend"] }

      it "#traintainer? returns false" do
        expect(subject.traintainer?(project, :tooling, labels)).to be_falsey
      end
    end

    context "when role is Backend Engineer, Analytics Instrumentation" do
      let(:role) { "Backend Engineer, Analytics Instrumentation" }
      let(:capabilities) { ["reviewer analytics_instrumentation"] }

      it "#reviewer? returns true" do
        expect(subject.reviewer?(project, :analytics_instrumentation, labels)).to be_truthy
      end

      it "#maintainer? returns false" do
        expect(subject.maintainer?(project, :analytics_instrumentation, labels)).to be_falsey
      end
    end
  end

  context "when having single capability" do
    let(:capabilities) { "reviewer backend" }

    it "#reviewer? supports one role per project" do
      expect(subject.reviewer?(project, :backend, labels)).to be_truthy
    end

    it "#traintainer? supports one role per project" do
      expect(subject.traintainer?(project, :database, labels)).to be_falsey
    end

    it "#maintainer? supports one role per project" do
      expect(subject.maintainer?(project, :frontend, labels)).to be_falsey
    end
  end

  context "when having capability in projects without categories" do
    let(:capabilities) { ["reviewer"] }

    it "#reviewer? returns true" do
      expect(subject.reviewer?(project, :none, labels)).to be_truthy
    end

    it "#traintainer? returns false" do
      expect(subject.traintainer?(project, :none, labels)).to be_falsey
    end

    it "#maintainer? returns false" do
      expect(subject.maintainer?(project, :none, labels)).to be_falsey
    end
  end

  describe "#projects" do
    context "when a project is uppercase" do
      let(:project) { "GITLAB" }

      it "is made lowercase" do
        expect(subject.projects).to eq("gitlab" => ["reviewer backend"])
      end
    end

    context "when a capability is uppercase" do
      let(:capabilities) { ["QA"] }

      it "is made lowercase" do
        expect(subject.projects).to eq("gitlab" => ["qa"])
      end
    end
  end

  describe "#local_hour" do
    around do |example|
      Timecop.freeze(Time.utc(2020, 6, 23, 10)) { example.run }
    end

    context "when author is given" do
      where(:tz_offset_hours, :expected_local_hour) do
        -12 | 22
        -10 | 0
        2 | 12
        4 | 14
        12 | 22
      end

      with_them do
        it "returns the correct local_hour" do
          expect(subject.local_hour).to eq(expected_local_hour)
        end
      end
    end
  end

  describe "#markdown_name" do
    context "when timezone info is not given" do
      before do
        options.delete("tz_offset_hours")
      end

      it "returns the markdown name without timezone info" do
        expect(subject.markdown_name).to eq(options["markdown_name"])
      end

      context "when markdown name is not provided in the first place" do
        # Make sure to delete it before calling subject to initialize it
        let!(:markdown_name) { options.delete("markdown_name") }

        it "returns the markdown name built from #initialize" do
          expect(subject.markdown_name).to eq(markdown_name)
        end
      end
    end

    it "returns markdown name with timezone info" do
      expect(subject.markdown_name).to eq("#{options["markdown_name"]} (UTC+2)")
    end

    context "when offset is 1.5" do
      let(:tz_offset_hours) { 1.5 }

      it "returns markdown name with timezone info, not truncated" do
        expect(subject.markdown_name).to eq("#{options["markdown_name"]} (UTC+1.5)")
      end
    end

    context "when author is given" do
      where(:tz_offset_hours, :author_offset, :diff_text) do
        -12 | -10 | "2 hours behind `@mario`"
        -10 | -12 | "2 hours ahead of `@mario`"
        -10 | 2 | "12 hours behind `@mario`"
        2 | 4 | "2 hours behind `@mario`"
        4 | 2 | "2 hours ahead of `@mario`"
        2 | 3 | "1 hour behind `@mario`"
        3 | 2 | "1 hour ahead of `@mario`"
        2 | 2 | "same timezone as `@mario`"
      end

      with_them do
        it "returns markdown name with timezone info" do
          author = described_class.new(options.merge("username" => "mario", "tz_offset_hours" => author_offset))

          floored_offset_hours = subject.__send__(:floored_offset_hours)
          utc_offset = floored_offset_hours >= 0 ? "+#{floored_offset_hours}" : floored_offset_hours

          expect(subject.markdown_name(author: author)).to eq("#{options["markdown_name"]} (UTC#{utc_offset}, #{diff_text})")
        end
      end
    end
  end
end

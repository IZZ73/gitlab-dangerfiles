# frozen_string_literal: true

require "spec_helper"

require "gitlab/dangerfiles/type_label_guesser"

RSpec.describe Gitlab::Dangerfiles::TypeLabelGuesser do
  subject(:type_label_guesser) { described_class.new }

  describe "#labels_from_changelog_categories" do
    context "with no categories given" do
      it "returns []" do
        expect(type_label_guesser.labels_from_changelog_categories([])).to eq([])
      end
    end

    context "with a single valid category given" do
      it "returns correct labels" do
        expect(type_label_guesser.labels_from_changelog_categories(["fixed"])).to eq(described_class::CHANGELOG_CATEGORY_TO_TYPE_LABEL[:fixed])
      end
    end

    context "with a single invalid category given" do
      it "returns correct labels" do
        expect(type_label_guesser.labels_from_changelog_categories(["foo"])).to eq([])
      end
    end

    context "with multiple categories given, with a single valid one" do
      it "returns correct labels" do
        expect(type_label_guesser.labels_from_changelog_categories(%w[foo fixed])).to eq(described_class::CHANGELOG_CATEGORY_TO_TYPE_LABEL[:fixed])
      end
    end

    context "with multiple categories given, with multiple single valid one" do
      it "returns correct labels" do
        expect(type_label_guesser.labels_from_changelog_categories(%w[added fixed])).to eq([])
      end
    end
  end
end

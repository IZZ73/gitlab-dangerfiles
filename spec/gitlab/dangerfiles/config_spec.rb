# frozen_string_literal: true

require "spec_helper"

require "gitlab/dangerfiles/config"

RSpec.describe Gitlab::Dangerfiles::Config do
  subject(:config) { described_class.new }

  describe "DEFAULT_CHANGES_SIZE_THRESHOLDS" do
    it { expect(described_class::DEFAULT_CHANGES_SIZE_THRESHOLDS).to eq(high: 2_000, medium: 500) }
  end

  describe "DEFAULT_COMMIT_MESSAGES_MAX_COMMITS_COUNT" do
    it { expect(described_class::DEFAULT_COMMIT_MESSAGES_MAX_COMMITS_COUNT).to eq(10) }
  end

  shared_examples "overridable config" do |config_name, default_value|
    describe "default value" do
      it { expect(config.public_send(config_name)).to eq(default_value) }
    end

    describe "overriding" do
      it "is possible to override a config directly" do
        config.public_send("#{config_name}=", new_config)

        expect(config.public_send(config_name)).to eq(new_config)
      end
    end
  end

  describe "#project_name" do
    before do
      allow(ENV).to receive(:[]).with("CI_PROJECT_NAME").and_return("the-project")
    end

    it_behaves_like "overridable config", :project_name, "the-project" do
      let(:new_config) { "my-other-project" }
    end
  end

  describe "#files_to_category" do
    before do
      allow(ENV).to receive(:[]).with("CI_PROJECT_NAME").and_return("my-project")
    end

    it_behaves_like "overridable config", :files_to_category, {} do
      let(:new_config) { { /foo/ => %w[bar baz] } }
    end
  end

  describe "#code_size_thresholds" do
    it_behaves_like "overridable config", :code_size_thresholds, described_class::DEFAULT_CHANGES_SIZE_THRESHOLDS do
      let(:new_config) do
        {
          high: config.code_size_thresholds[:high] / 2,
          medium: config.code_size_thresholds[:medium] / 2,
        }
      end
    end
  end

  describe "#max_commits_count" do
    it_behaves_like "overridable config", :max_commits_count, described_class::DEFAULT_COMMIT_MESSAGES_MAX_COMMITS_COUNT do
      let(:new_config) { 30 }
    end
  end

  describe "#disabled_roulette_categories" do
    it_behaves_like "overridable config", :disabled_roulette_categories, [] do
      let(:new_config) { [:ux] }
    end
  end

  describe "#included_optional_codeowners_sections_for_roulette" do
    it_behaves_like "overridable config", :included_optional_codeowners_sections_for_roulette, [] do
      let(:new_config) { %w[foo bar] }
    end
  end

  describe "#excluded_required_codeowners_sections_for_roulette" do
    it_behaves_like "overridable config", :excluded_required_codeowners_sections_for_roulette, [] do
      let(:new_config) { %w[foo bar] }
    end
  end
end

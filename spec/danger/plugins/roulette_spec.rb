# frozen_string_literal: true

require "spec_helper"

RSpec.describe Danger::Roulette do
  include_context "with dangerfile"

  around do |example|
    Timecop.travel(Time.utc(2020, 06, 22, 10)) { example.run }
  end

  it "is a plugin" do
    expect(described_class < Danger::Plugin).to be_truthy
  end

  let(:project) { "gitlab" }
  let(:author) { Gitlab::Dangerfiles::Teammate.new("username" => "johndoe") }
  let(:mr_source_branch) { "a-branch" }
  let(:backend_available) { true }
  let(:backend_tz_offset_hours) { 2.0 }
  let(:backend_maintainer_project) { { "gitlab" => "maintainer backend" } }
  let(:backend_maintainer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "backend-maintainer",
      "name" => "Backend maintainer",
      "role" => "Backend engineer",
      "projects" => backend_maintainer_project,
      "available" => backend_available,
      "tz_offset_hours" => backend_tz_offset_hours,
    )
  end

  let(:another_backend_maintainer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "another-backend-maintainer",
      "name" => "Another Backend Maintainer",
      "role" => "Backend engineer",
      "projects" => { "gitlab" => "maintainer backend" },
      "available" => backend_available,
      "tz_offset_hours" => backend_tz_offset_hours,
    )
  end

  let(:frontend_reviewer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "frontend-reviewer",
      "name" => "Frontend reviewer",
      "role" => "Frontend engineer",
      "projects" => { "gitlab" => "reviewer frontend" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:frontend_maintainer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "frontend-maintainer",
      "name" => "Frontend maintainer",
      "role" => "Frontend engineer",
      "projects" => { "gitlab" => "maintainer frontend" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:ux_reviewer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "ux-reviewer",
      "name" => "UX reviewer",
      "role" => "Product Designer",
      "projects" => { "gitlab" => "reviewer ux" },
      "specialty" => "Create: Source Code",
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:software_engineer_in_test) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "software-engineer-in-test",
      "name" => "Software Engineer in Test",
      "role" => "Software Engineer in Test, Create:Source Code",
      "projects" => { "gitlab" => "maintainer qa", "gitlab-qa" => "maintainer" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:software_engineer_in_import_integrate_fe) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "software-engineer-in-import-and-integrate-fe",
      "name" => "Software Engineer in Import and Integrate FE",
      "role" => "Frontend Engineer, Manage:Import and Integrate",
      "projects" => { "gitlab" => "reviewer frontend" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:software_engineer_in_import_integrate_be) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "software-engineer-in-import-and-integrate-be",
      "name" => "Software Engineer in Import and Integrate BE",
      "role" => "Backend Engineer, Manage:Import and Integrate",
      "projects" => { "gitlab" => "reviewer backend" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:tooling_reviewer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "eng-prod-reviewer",
      "name" => "EP engineer",
      "role" => "Engineering Productivity",
      "projects" => { "gitlab" => "reviewer backend" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:ci_template_reviewer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "ci-template-maintainer",
      "name" => "CI Template engineer",
      "role" => '~"ci::templates"',
      "projects" => { "gitlab" => "reviewer ci_template" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:analytics_instrumentation_reviewer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "analytics-instrumentation-reviewer",
      "name" => "PI engineer",
      "role" => "Backend Engineer, Analytics: Analytics Instrumentation",
      "projects" => { "gitlab" => "reviewer analytics_instrumentation" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:import_and_integrate_backend_reviewer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "import-and-integrate-backend-reviewer",
      "name" => "Import and Integrate BE engineer",
      "role" => "Backend Engineer, Manage:Import and Integrate",
      "projects" => { "gitlab" => "reviewer backend" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:import_and_integrate_frontend_reviewer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "import-and-integrate-frontend-reviewer",
      "name" => "Import and Integrate FE engineer",
      "role" => "Frontend Engineer, Manage:Import and Integrate",
      "projects" => { "gitlab" => "reviewer frontend" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:workhorse_reviewer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "workhorse-reviewer",
      "name" => "Workhorse reviewer",
      "role" => "Backend engineer",
      "projects" => { "gitlab-workhorse" => "reviewer" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:workhorse_maintainer) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "workhorse-maintainer",
      "name" => "Workhorse maintainer",
      "role" => "Backend engineer",
      "projects" => { "gitlab-workhorse" => "maintainer" },
      "available" => true,
      "tz_offset_hours" => 2.0,
    )
  end

  let(:teammates) do
    [
      backend_maintainer.to_h,
      frontend_maintainer.to_h,
      frontend_reviewer.to_h,
      ux_reviewer.to_h,
      software_engineer_in_test.to_h,
      tooling_reviewer.to_h,
      ci_template_reviewer.to_h,
      workhorse_reviewer.to_h,
      workhorse_maintainer.to_h,
      analytics_instrumentation_reviewer.to_h,
      import_and_integrate_backend_reviewer.to_h,
      import_and_integrate_frontend_reviewer.to_h,
      software_engineer_in_import_integrate_fe.to_h,
      software_engineer_in_import_integrate_be.to_h,
    ]
  end

  let(:teammate_json) { teammates.to_json }
  let(:teammate_pedroms) { instance_double(Gitlab::Dangerfiles::Teammate, name: "Pedro") }

  subject(:roulette) { dangerfile.roulette }

  before do
    allow(dangerfile.helper).to receive(:mr_source_branch).and_return(mr_source_branch)
  end

  describe "Spin#==" do
    it "compares Spin attributes" do
      spin1 = described_class::Spin.new(:backend, frontend_reviewer, frontend_maintainer, false)
      spin2 = described_class::Spin.new(:backend, frontend_reviewer, frontend_maintainer, false)
      spin3 = described_class::Spin.new(:backend, frontend_reviewer, frontend_maintainer, true)
      spin4 = described_class::Spin.new(:backend, frontend_reviewer, backend_maintainer, false)
      spin5 = described_class::Spin.new(:backend, backend_maintainer, frontend_maintainer, false)
      spin6 = described_class::Spin.new(:frontend, frontend_reviewer, frontend_maintainer, false)

      expect(spin1).to eq(spin2)
      expect(spin1).not_to eq(spin3)
      expect(spin1).not_to eq(spin4)
      expect(spin1).not_to eq(spin5)
      expect(spin1).not_to eq(spin6)
    end
  end

  describe "#prepare_categories" do
    let(:mr_labels) { [] }

    before do
      allow(dangerfile.helper).to receive(:mr_labels).and_return(mr_labels)
    end

    context "when there is UX label and Community contribution" do
      let(:mr_labels) { ["UX", "Community contribution"] }

      it "adds :ux to categories" do
        expect(roulette.prepare_categories([])).to include(:ux)
      end
    end

    context "when there is UX label" do
      let(:mr_labels) { ["UX"] }

      before do
        allow(dangerfile.helper).to receive(:mr_author).and_return(author.username)
        allow(roulette).to receive(:config_project_name).and_return(project)
        allow(roulette).to receive(:teammate_pedroms).and_return(teammate_pedroms)

        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return(body: teammate_json)
      end

      it "does not add :ux because there is no group" do
        expect(roulette.prepare_categories([])).not_to include(:ux)
      end
    end
  end

  describe "#spin" do
    let(:mr_labels) { ["backend", "devops::create"] }
    let(:spins) do
      # Stub the request at the latest time so that we can modify the raw data, e.g. available fields.
      WebMock
        .stub_request(:get, described_class::ROULETTE_DATA_URL)
        .to_return(body: teammate_json)

      subject.spin(project, categories)
    end

    before do
      allow(dangerfile.helper).to receive(:mr_author).and_return(author.username)
      allow(dangerfile.helper).to receive(:mr_labels).and_return(mr_labels)
    end

    context "when change contains backend category" do
      let(:categories) { [:backend] }

      it "assigns backend reviewer and maintainer" do
        expect(spins[0].reviewer).to eq(tooling_reviewer)
        expect(spins[0].maintainer).to eq(backend_maintainer)
        expect(spins).to eq([described_class::Spin.new(:backend, tooling_reviewer, backend_maintainer, false)])
      end

      context "when teammate is not available" do
        let(:backend_available) { false }

        it "assigns backend reviewer and no maintainer" do
          expect(spins).to eq([described_class::Spin.new(:backend, tooling_reviewer, nil, false)])
        end
      end

      context "when no reviewers are available and there are multiple maintainers available" do
        let(:teammates) do
          [
            backend_maintainer.to_h,
            another_backend_maintainer.to_h,
          ]
        end

        it "assigns backend reviewer from maintainer and anther maintainer as maintainer" do
          expect(spins).to eq([described_class::Spin.new(:backend, another_backend_maintainer, backend_maintainer, false)])
        end
      end

      context "when no reviewers are available and there is one maintainer available" do
        let(:teammates) do
          [
            backend_maintainer.to_h,
          ]
        end

        it "assigns backend maintainer and no reviewer" do
          expect(spins).to eq([described_class::Spin.new(:backend, nil, backend_maintainer, false)])
        end
      end
    end

    context "when change contains frontend category" do
      let(:categories) { [:frontend] }

      it "assigns frontend reviewer and maintainer" do
        expect(spins).to eq([described_class::Spin.new(:frontend, frontend_reviewer, frontend_maintainer, false)])
      end
    end

    context "when change contains many categories" do
      let(:categories) { [:frontend, :test, :qa, :tooling, :ci_template, :backend, :ux, :none] }

      it "has a deterministic sorting order" do
        expect(spins.map(&:category)).to eq categories.sort_by(&:to_s)
      end
    end

    context "when change contains QA category" do
      let(:categories) { [:qa] }

      it "assigns QA maintainer" do
        expect(spins).to eq([described_class::Spin.new(:qa, nil, software_engineer_in_test, false)])
      end
    end

    context "when change contains UX category" do
      let(:categories) { [:ux] }

      it "assigns UX reviewer and maintainer is optional" do
        expect(spins).to eq([described_class::Spin.new(:ux, ux_reviewer, nil, :maintainer)])
      end

      context "when it is a wider community contribution without a group" do
        let(:mr_labels) { ["Community contribution"] }

        before do
          allow(roulette).to receive(:teammate_pedroms).and_return(teammate_pedroms)
        end

        it "assigns Pedro" do
          expect(spins).to eq([described_class::Spin.new(:ux, teammate_pedroms, nil, :maintainer)])
        end
      end

      context "when it is a wider community contribution with source code group" do
        let(:mr_labels) { ["Community contribution", "group::source code"] }

        it "assigns source code UX reviewer" do
          expect(spins).to eq([described_class::Spin.new(:ux, ux_reviewer, nil, :maintainer)])
        end
      end
    end

    context "when change contains Analytics Instrumentation category" do
      let(:categories) { [:analytics_instrumentation] }

      it "assigns Analytics Instrumentation reviewer" do
        expect(spins).to eq([described_class::Spin.new(:analytics_instrumentation, analytics_instrumentation_reviewer, backend_maintainer, :maintainer)])
      end
    end

    shared_examples "assigns Import and Integrate Backend review correctly" do
      it "assigns Import and Integrate Backend review, and no maintainer" do
        expect(spins).to eq([described_class::Spin.new(:import_integrate_be, import_and_integrate_backend_reviewer, nil, :maintainer)])
      end
    end

    shared_examples "assigns Import and Integrate Frontend review correctly" do
      it "assigns Import and Integrate Frontend review, and no maintainer" do
        expect(spins).to eq([described_class::Spin.new(:import_integrate_fe, import_and_integrate_frontend_reviewer, nil, :maintainer)])
      end
    end

    context "when change contains Import and Integrate Backend category" do
      let(:categories) { [:import_integrate_be] }

      it_behaves_like "assigns Import and Integrate Backend review correctly"

      context "when author is an Import and Integrate Backend team member" do
        let!(:author) { software_engineer_in_import_integrate_be }

        it "does not assign a reviewer" do
          expect(spins).to be_empty
        end
      end

      context "when author is an Import and Integrate Frontend team member" do
        let!(:author) { software_engineer_in_import_integrate_fe }

        it_behaves_like "assigns Import and Integrate Backend review correctly"
      end
    end

    context "when change contains Import and Integrate Frontend category" do
      let(:categories) { [:import_integrate_fe] }

      it_behaves_like "assigns Import and Integrate Frontend review correctly"

      context "when author is an Import and Integrate Frontend team member" do
        let!(:author) { software_engineer_in_import_integrate_fe }

        it "does not assign a reviewer" do
          expect(spins).to be_empty
        end
      end

      context "when author is an Import and Integrate Backend team member" do
        let!(:author) { software_engineer_in_import_integrate_be }

        it_behaves_like "assigns Import and Integrate Frontend review correctly"
      end
    end

    context "when change contains QA category and another category" do
      let(:categories) { [:backend, :qa] }

      it "assigns QA maintainer" do
        expect(spins).to eq([described_class::Spin.new(:backend, tooling_reviewer, backend_maintainer, false), described_class::Spin.new(:qa, nil, software_engineer_in_test, :maintainer)])
      end

      context "and author is an SET" do
        let!(:author) { Gitlab::Dangerfiles::Teammate.new("username" => software_engineer_in_test.username) }

        it "assigns QA reviewer" do
          expect(spins).to eq([described_class::Spin.new(:backend, tooling_reviewer, backend_maintainer, false), described_class::Spin.new(:qa, nil, nil, false)])
        end
      end
    end

    context "when change contains Engineering Productivity category" do
      let(:categories) { [:tooling] }

      it "assigns Engineering Productivity reviewer and fallback to backend maintainer" do
        expect(spins).to eq([described_class::Spin.new(:tooling, tooling_reviewer, backend_maintainer, false)])
      end
    end

    context "when change contains CI/CD Template category" do
      let(:categories) { [:ci_template] }

      it "assigns CI/CD Template reviewer and fallback to backend maintainer" do
        expect(spins).to eq([described_class::Spin.new(:ci_template, ci_template_reviewer, backend_maintainer, false)])
      end
    end

    context "when change contains test category" do
      let(:categories) { [:test] }

      it "assigns corresponding SET" do
        expect(spins).to eq([described_class::Spin.new(:test, software_engineer_in_test, nil, :maintainer)])
      end
    end

    context "when project has no categories" do
      let(:project) { "gitlab-workhorse" }
      let(:categories) { [:none] }

      it "assigns project reviewer & maintainer" do
        expect(spins).to eq([described_class::Spin.new(:none, workhorse_reviewer, workhorse_maintainer, false)])
      end
    end

    context "when project has legacy nil category" do
      let(:project) { "gitlab-workhorse" }
      let(:categories) { [nil] }

      it "assigns project reviewer & maintainer" do
        expect(spins).to eq([described_class::Spin.new(:none, workhorse_reviewer, workhorse_maintainer, false)])
      end
    end

    context "when project has a category that's uppercase" do
      let(:categories) { [:QA] }

      it "is made lowercase" do
        expect(spins).to eq([described_class::Spin.new(:qa, nil, software_engineer_in_test, false)])
      end
    end
  end

  RSpec::Matchers.define :match_teammates do |expected|
    match do |actual|
      expected.each do |expected_person|
        actual_person_found = actual.find { |actual_person| actual_person.name == expected_person.username }

        actual_person_found &&
        actual_person_found.name == expected_person.name &&
        actual_person_found.role == expected_person.role &&
        actual_person_found.projects == expected_person.projects
      end
    end
  end

  describe "#company_members" do
    subject(:company_members) { roulette.__send__(:company_members) }

    context "HTTP failure" do
      before do
        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return(status: [431, "Request Header Fields Too Large"])
      end

      it "warns and return an empty list" do
        expect(company_members).to eq([])
        expect(roulette.warnings).to include("HTTPError: Failed to read #{described_class::ROULETTE_DATA_URL}: 431 Request Header Fields Too Larg.")
      end
    end

    context "JSON failure" do
      before do
        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return(body: "INVALID JSON")
      end

      it "warns and return an empty list" do
        expect(company_members).to eq([])
        expect(roulette.warnings).to include(/Failed to parse/)
      end
    end

    context "success" do
      before do
        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return(body: teammate_json)
      end

      it "returns an array of teammates" do
        is_expected.to match_teammates([
          backend_maintainer,
          frontend_reviewer,
          frontend_maintainer,
          software_engineer_in_test,
          tooling_reviewer,
          ci_template_reviewer,
        ])
        expect(roulette.warnings).to eq([])
      end

      it "memoizes the result" do
        expect(company_members.object_id).to eq(roulette.__send__(:company_members).object_id)
      end
    end

    context "redirection" do
      let(:location) { "http://example.com?query" }
      let(:clean_location) { "http://example.com" }

      before do
        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return({ status: 302, headers: { location: location } }, { body: teammate_json })
      end

      it "warns and return an empty list" do
        expect(company_members).to eq([])
        expect(roulette.warnings).to include("Redirection detected: #{clean_location}.")
      end
    end
  end

  describe "#project_team" do
    subject { roulette.__send__(:project_team, "gitlab-qa") }

    before do
      WebMock
        .stub_request(:get, described_class::ROULETTE_DATA_URL)
        .to_return(body: teammate_json)
    end

    it "filters team by project_name" do
      is_expected.to match_teammates([
        software_engineer_in_test,
      ])
    end
  end

  describe "#spin_for_person" do
    let(:person_tz_offset_hours) { 0.0 }
    let(:person1) do
      Gitlab::Dangerfiles::Teammate.new(
        "username" => "user1",
        "available" => true,
        "tz_offset_hours" => person_tz_offset_hours,
      )
    end

    let(:person2) do
      Gitlab::Dangerfiles::Teammate.new(
        "username" => "user2",
        "available" => true,
        "tz_offset_hours" => person_tz_offset_hours,
      )
    end

    let(:author) do
      Gitlab::Dangerfiles::Teammate.new(
        "username" => "johndoe",
        "available" => true,
        "tz_offset_hours" => 0.0,
      )
    end

    let(:unavailable) do
      Gitlab::Dangerfiles::Teammate.new(
        "username" => "janedoe",
        "available" => false,
        "tz_offset_hours" => 0.0,
      )
    end

    before do
      allow(dangerfile.helper).to receive(:mr_author).and_return(author.username)
    end

    (-4..4).each do |utc_offset|
      context "when local hour for person is #{10 + utc_offset} (offset: #{utc_offset})" do
        let(:person_tz_offset_hours) { utc_offset }

        it "returns a random person" do
          persons = [person1, person2]

          selected = subject.__send__(:spin_for_person, persons)

          expect(persons.map(&:username)).to include(selected.username)
        end
      end
    end

    ((-12..-5).to_a + (5..12).to_a).each do |utc_offset|
      context "when local hour for person is #{10 + utc_offset} (offset: #{utc_offset})" do
        let(:person_tz_offset_hours) { utc_offset }

        it "returns a random person or nil" do
          persons = [person1, person2]

          selected = subject.__send__(:spin_for_person, persons)

          expect(persons.map(&:username)).to include(selected.username)
        end
      end
    end

    it "excludes unavailable persons" do
      expect(subject.__send__(:spin_for_person, [unavailable])).to be_nil
    end

    it "excludes mr.author" do
      expect(subject.__send__(:spin_for_person, [author])).to be_nil
    end
  end

  describe "#codeowners_approvals" do
    context "when gitlab_helper is not available" do
      before do
        allow(dangerfile.helper).to receive(:gitlab_helper).and_return(nil)
      end

      it "returns an empty array" do
        expect(subject.codeowners_approvals).to be_empty
      end
    end

    context "when gitlab_helper is available" do
      before do
        allow(dangerfile.helper).to receive(:mr_author).and_return("username" => "johndoe")
        allow(dangerfile.helper).to receive(:mr_iid).and_return("123")
        allow(dangerfile.helper).to receive(:mr_target_project_id).and_return("1234")
        allow(dangerfile.helper.config).to receive(:project_name).and_return("gitlab")

        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return(body: teammate_json)

        allow(dangerfile.helper).to receive(:mr_approval_state).and_return(return_json)
      end

      context "when there is a single approval rule" do
        let(:rule) do
          {
            "id" => 333,
            "name" => "*.rb",
            "rule_type" => "code_owner",
            "eligible_approvers" => [valid_reviewer, invalid_reviewer],
            "approvals_required" => number_of_approvals_required,
            "section" => "Backend",
            "code_owner" => true,
          }
        end

        let(:valid_reviewer) do
          {
            "id" => 123,
            "username" => backend_maintainer.username,
            "name" => backend_maintainer.name,
            "state" => "active",
          }
        end

        let(:invalid_reviewer) do
          {
            "id" => 321,
            "username" => "not-reviewer",
            "name" => "Not Reviewer",
            "state" => "active",
          }
        end

        let(:return_json) { { "rules" => [rule] } }

        context "no required approvals are needed" do
          let(:number_of_approvals_required) { 0 }

          it "returns an empty array" do
            expect(subject.codeowners_approvals).to be_empty
          end
        end

        context "one required approval is needed" do
          let(:number_of_approvals_required) { 1 }

          before do
            allow(subject).to receive(:spin_for_approver_fallback).with(rule).and_call_original
          end

          it "returns the required approval with a valid reviewer" do
            codeowners_approvals = subject.codeowners_approvals

            expect(codeowners_approvals.count).to eq(1)

            expect(codeowners_approvals.first.category).to eq(:Backend)
            expect(codeowners_approvals.first.spin.maintainer.username).to eq(backend_maintainer.username)

            expect(subject).not_to have_received(:spin_for_approver_fallback)
          end

          context "when there are no valid reviewers available" do
            let(:backend_available) { false }

            it "returns the required approval with a fallback reviewer" do
              codeowners_approvals = subject.codeowners_approvals

              expect(codeowners_approvals.count).to eq(1)
              expect(codeowners_approvals.first.spin.maintainer.username).to eq(valid_reviewer["username"]).or(invalid_reviewer["username"])

              expect(subject).to have_received(:spin_for_approver_fallback)
            end
          end

          context "when no one is in the right project" do
            let(:backend_maintainer_project) { {} }

            it "returns the required approval with a fallback reviewer" do
              codeowners_approvals = subject.codeowners_approvals

              expect(codeowners_approvals.count).to eq(1)
              expect(codeowners_approvals.first.spin.maintainer.username).to eq(valid_reviewer["username"]).or(invalid_reviewer["username"])

              expect(subject).to have_received(:spin_for_approver_fallback)
            end
          end
        end
      end

      context "when there are multiple approval rules" do
        let(:number_of_approvals_required) { 1 }
        let(:rule_for_ruby_files) do
          {
            "id" => 333,
            "name" => "*.rb",
            "rule_type" => "code_owner",
            "eligible_approvers" => [valid_reviewer_for_ruby_files],
            "approvals_required" => number_of_approvals_required,
            "section" => "Backend",
            "code_owner" => true,
          }
        end

        let(:rule_for_rake_files) do
          {
            "id" => 551,
            "name" => "*.rake",
            "rule_type" => "code_owner",
            "eligible_approvers" => [valid_reviewer_for_rake_files],
            "approvals_required" => number_of_approvals_required,
            "section" => "Backend",
            "code_owner" => true,
          }
        end

        let(:rule_for_js_files) do
          {
            "id" => 551,
            "name" => "*.js",
            "rule_type" => "code_owner",
            "eligible_approvers" => [valid_reviewer_for_js_files],
            "approvals_required" => number_of_approvals_required,
            "section" => "Frontend",
            "code_owner" => true,
          }
        end

        let(:valid_reviewer_for_ruby_files) do
          {
            "id" => 123,
            "username" => backend_maintainer.username,
            "name" => backend_maintainer.name,
            "state" => "active",
          }
        end

        let(:valid_reviewer_for_rake_files) do
          {
            "id" => 456,
            "username" => another_backend_maintainer.username,
            "name" => another_backend_maintainer.name,
            "state" => "active",
          }
        end

        let(:valid_reviewer_for_js_files) do
          {
            "id" => 789,
            "username" => frontend_maintainer.username,
            "name" => frontend_maintainer.name,
            "state" => "active",
          }
        end

        context "when the same section has multiple approval rules" do
          let(:return_json) { { "rules" => [rule_for_ruby_files, rule_for_rake_files] } }

          context "and the rules require different approvers" do
            it "returns the required approvals from different approvers" do
              expect(subject.codeowners_approvals.count).to eq(2)
              expect(subject.codeowners_approvals[0].category).to eq(:Backend)
              expect(subject.codeowners_approvals[0].spin.maintainer.username).to eq(backend_maintainer.username)
              expect(subject.codeowners_approvals[1].category).to eq(:Backend)
              expect(subject.codeowners_approvals[1].spin.maintainer.username).to eq(another_backend_maintainer.username)
            end
          end

          context "and the rules share the same approvers" do
            let(:valid_reviewer_for_rake_files) do
              {
                "id" => 456,
                "username" => backend_maintainer.username,
                "name" => backend_maintainer.name,
                "state" => "active",
              }
            end

            it "returns just 1 approval from the 2 rules" do
              expect(subject.codeowners_approvals.count).to eq(1)
            end
          end
        end

        context "when approval rules include different sections" do
          let(:return_json) { { "rules" => [rule_for_ruby_files, rule_for_js_files] } }

          it "returns the required approvals from different section" do
            expect(subject.codeowners_approvals.count).to eq(2)
            expect(subject.codeowners_approvals[0].category).to eq(:Backend)
            expect(subject.codeowners_approvals[0].spin.maintainer.username).to eq(backend_maintainer.username)
            expect(subject.codeowners_approvals[1].category).to eq(:Frontend)
            expect(subject.codeowners_approvals[1].spin.maintainer.username).to eq(frontend_maintainer.username)
          end
        end

        context "when there is a generic codeowners rule" do
          let(:rule_for_generic_codeowners) do
            {
              "id" => 777,
              "name" => "*",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_js_files],
              "approvals_required" => 1,
              "section" => "codeowners",
              "code_owner" => true,
            }
          end

          let(:return_json) { { "rules" => [rule_for_ruby_files, rule_for_generic_codeowners] } }

          it "returns the required approvals without the generic one" do
            expect(subject.codeowners_approvals.count).to eq(1)
            expect(subject.codeowners_approvals[0].category).to eq(:Backend)
            expect(subject.codeowners_approvals[0].spin.maintainer.username).to eq(backend_maintainer.username)
          end
        end

        context "when there is a codeowners rule without a section name" do
          let(:rule_for_codeowners_without_section) do
            {
              "id" => 777,
              "name" => "/path/or/glob/**/*.rb",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_ruby_files],
              "approvals_required" => 1,
              "section" => "codeowners",
              "code_owner" => true,
            }
          end

          let(:return_json) { { "rules" => [rule_for_codeowners_without_section] } }

          it "returns the required approvals using name as the category" do
            expect(subject.codeowners_approvals.count).to eq(1)
            expect(subject.codeowners_approvals[0].category).to eq(:"`/path/or/glob/**/*.rb`")
            expect(subject.codeowners_approvals[0].spin.maintainer.username).to eq(backend_maintainer.username)
          end
        end

        context "when including an optional codeowners rule via config" do
          let(:number_of_approvals_required) { 0 }
          let(:rule_for_ruby_files) do
            {
              "id" => 333,
              "name" => "*.rb",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_ruby_files],
              "approvals_required" => number_of_approvals_required,
              "section" => "Backend",
              "code_owner" => true,
            }
          end

          let(:return_json) { { "rules" => [rule_for_ruby_files] } }

          it "returns the appropriate approval rule" do
            allow(dangerfile.helper.config).to receive(:included_optional_codeowners_sections_for_roulette).and_return(["Backend"])
            expect(subject.codeowners_approvals.count).to eq(1)
            expect(subject.codeowners_approvals[0].category).to eq(:Backend)
          end
        end

        context "when excluding a required codeowners rule via config" do
          let(:number_of_approvals_required) { 1 }
          let(:rule_for_ruby_files) do
            {
              "id" => 333,
              "name" => "*.rb",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_ruby_files],
              "approvals_required" => number_of_approvals_required,
              "section" => "Backend",
              "code_owner" => true,
            }
          end

          let(:return_json) { { "rules" => [rule_for_ruby_files] } }

          it "does not return the approval rule" do
            allow(dangerfile.helper.config).to receive(:excluded_required_codeowners_sections_for_roulette).and_return(["Backend"])
            expect(subject.codeowners_approvals.count).to eq(0)
          end
        end

        context "when including and excluding the same codeowners rule via config" do
          let(:number_of_approvals_required) { 1 }
          let(:rule_for_ruby_files) do
            {
              "id" => 333,
              "name" => "*.rb",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_ruby_files],
              "approvals_required" => number_of_approvals_required,
              "section" => "Backend",
              "code_owner" => true,
            }
          end

          let(:return_json) { { "rules" => [rule_for_ruby_files] } }

          it "treats the rule as excluded" do
            allow(dangerfile.helper.config).to receive(:excluded_required_codeowners_sections_for_roulette).and_return(["Backend"])
            allow(dangerfile.helper.config).to receive(:included_optional_codeowners_sections_for_roulette).and_return(["Backend"])
            expect(subject.codeowners_approvals.count).to eq(0)
          end
        end
      end
    end
  end
end
